var NAVTREE =
[
  [ "cdk", "index.html", [
    [ "libcdk", "md_README.html", null ],
    [ "Classes", "annotated.html", [
      [ "Class List", "annotated.html", "annotated_dup" ],
      [ "Class Hierarchy", "hierarchy.html", "hierarchy" ],
      [ "Class Members", "functions.html", [
        [ "All", "functions.html", "functions_dup" ],
        [ "Functions", "functions_func.html", "functions_func" ],
        [ "Variables", "functions_vars.html", null ]
      ] ]
    ] ],
    [ "Files", null, [
      [ "File List", "files.html", "files" ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
".html",
"classcdk_1_1composite__node.html#ab0ee2b262c7b6b55a9ddb052be60b648",
"classcdk_1_1postfix__ix86__emitter.html#ad6da0a3ed460b9f64eee3031418b3769"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';