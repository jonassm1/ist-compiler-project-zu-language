%{
// $Id: zu_parser.y,v 1.8 2016/04/15 15:34:55 ist173891 Exp $
//-- don't change *any* of these: if you do, you'll break the compiler.
#include <cdk/compiler.h>
#include "ast/all.h"
#define LINE       compiler->scanner()->lineno()
#define yylex()    compiler->scanner()->scan()
#define yyerror(s) compiler->scanner()->error(s)
#define YYPARSE_PARAM_TYPE std::shared_ptr<cdk::compiler>
#define YYPARSE_PARAM      compiler
//-- don't change *any* of these --- END!
#include <string>
std::string str_tmp;
%}

%union {
  int                   i;	/* integer value */
  double                d;
  std::string          *s;	/* symbol name or string literal */
  cdk::basic_node      *node;	/* node pointer */
  cdk::sequence_node   *sequence;
  cdk::expression_node *expression; /* expression nodes */
  zu::lvalue_node  *lvalue;
};

%token <i> tINTEGER tHEX
%token <s> tIDENTIFIER tSTRING
%token <d> tREAL
%token tNO tAND tOR tCONTINUE tBREAK tRETURN
%token '-' '(' ')' '<' '>' '=' '+' '*' '/' '%' ';' '{' ','
%token '}' '.' '?' '[' ']' '$' '@' '!'


%right '='
%left tGE tLE tEQ tNE '>' '<'
%left '+' '-'
%left '*' '/' '%'
%left '#' '%' '$'
%nonassoc '(' ')' '[' ']'
%nonassoc tUNARY

%type <node> /*stmt program*/ file declaration variable instruction
%type <sequence> /*list*/ declarations variables expressions
%type <expression> expr
%type <lvalue> lval
%type <s> str

%{
//-- The rules below will be included in yyparse, the main parsing function.
%}
%%
/*
program	: tBEGIN list tEND { compiler->ast(new zu::program_node(LINE, $2)); }
	      ;

list : stmt	     { $$ = new cdk::sequence_node(LINE, $1); }
	   | list stmt { $$ = new cdk::sequence_node(LINE, $2, $1); }
	   ;

stmt : expr ';'                                   { $$ = new zu::evaluation_node(LINE, $1); }
 	   | tPRINT expr ';'                            { $$ = new zu::print_node(LINE, $2); }
     | tREAD lval ';'                             { $$ = new zu::read_node(LINE); }
     | tFOR '(' list ';' list ';' list ')' stmt   { $$ = new zu::for_node(LINE, $3, $5, $7, $9); }
     | tIF '(' expr ')' stmt %prec tIFX           { $$ = new zu::if_node(LINE, $3, $5); }
     | tIF '(' expr ')' stmt tELSE stmt           { $$ = new zu::if_else_node(LINE, $3, $5, $7); }
     | '{' list '}'                               { $$ = $2; }
     ;

expr : tINTEGER                { $$ = new cdk::integer_node(LINE, $1); }
	   | tREAL                   { $$ = new cdk::double_node(LINE, $1); }
	   | tSTRING                 { $$ = new cdk::string_node(LINE, $1); }
     | '-' expr %prec tUNARY   { $$ = new cdk::neg_node(LINE, $2); }
     | expr '+' expr	         { $$ = new cdk::add_node(LINE, $1, $3); }
     | expr '-' expr	         { $$ = new cdk::sub_node(LINE, $1, $3); }
     | expr '*' expr	         { $$ = new cdk::mul_node(LINE, $1, $3); }
     | expr '/' expr	         { $$ = new cdk::div_node(LINE, $1, $3); }
     | expr '%' expr	         { $$ = new cdk::mod_node(LINE, $1, $3); }
     | expr '<' expr	         { $$ = new cdk::lt_node(LINE, $1, $3); }
     | expr '>' expr	         { $$ = new cdk::gt_node(LINE, $1, $3); }
     | expr tGE expr	         { $$ = new cdk::ge_node(LINE, $1, $3); }
     | expr tLE expr           { $$ = new cdk::le_node(LINE, $1, $3); }
     | expr tNE expr	         { $$ = new cdk::ne_node(LINE, $1, $3); }
     | expr tEQ expr	         { $$ = new cdk::eq_node(LINE, $1, $3); }
     //| expr tAND expr          { $$ = new}
     //| expr tOR expr           { $$ = new}
     | '(' expr ')'            { $$ = $2; }
     | '[' expr ']'            { $$ = $2; }
     | '+' expr %prec '*'      { $$ = $2; }
     | '-' expr %prec '*'      { $$ = new cdk::neg_node(LINE, $2); }
     | tNO expr                { $$ = $2; }
     | expr '?'                { $$ = $1; }
     | lval                    { $$ = new zu::rvalue_node(LINE, $1); }  //FIXME
     | lval '=' expr           { $$ = new zu::assignment_node(LINE, $1, $3); }
     ;

lval : tIDENTIFIER             { $$ = new zu::lvalue_node(LINE, $1); }
     ;
*/
//----------------------------------------------------------------------------//

file : declarations
     ;

declarations : declaration                { $$ = new cdk::sequence_node(LINE, $1); }
             | declarations declaration   { $$ = new cdk::sequence_node(LINE, $2, $1); }
             ;

declaration : variable ';'                {}
            | function
            ;

variable : type identifier
         | type identifier variableoptionals
         ;

markers : '!'
        | '?'
        ;

variableoptionals : markers
                | markers '=' expr
                | '=' expr
                ;

function : functiontype identifier markers '(' variables ')' functionoptions
         | functiontype identifier markers '(' ')' functionoptions
         | functiontype identifier '(' variables ')' functionoptions
         | functiontype identifier '(' ')' functionoptions
         | functiontype identifier markers '(' variables ')'
         | functiontype identifier markers '(' ')'
         | functiontype identifier '(' variables ')'
         | functiontype identifier '(' ')'
         ;

functiontype : type
             | '!'
             ;

functionoptions : '=' literal
                | '=' literal body
                | body
                ;

variables : variable                { $$ = new cdk::sequence_node(LINE, $1); }
          | variable ',' variables  { $$ = new cdk::sequence_node(LINE, $1, $3); }
          ;

type : '#'
      | '%'
      | '$'
      | '<' type '>'
      ;

body : block
     ;

block : '{' declarations instruction '}'
      ;

instruction : expr ';'            { $$ = new zu::evaluation_node(LINE, $1); }
            | expr '!'
            | expr '!' '!'
            | tBREAK              { $$ = new zu::break_node(LINE); }
            | tCONTINUE           { $$ = new zu::continue_node(LINE); }
            | tRETURN             { $$ = new zu::return_node(LINE); }
            | conditionalinstruction
            | iterationinstruction
            | block
            ;

conditionalinstruction : '[' expr ']' '#' instruction
                       | '[' expr ']' '?' instruction
                       | '[' expr ']' '?' instruction ':' instruction
                       ;

iterationinstruction : '[' iterationoptions1 ']' instruction
                     | '[' iterationoptions2 ']' instruction
                     ;

optionalvariables :
                  | variables
                  ;

optionalexpressions :
                    | expressions
                    ;

iterationoptions1 : optionalvariables ';' optionalexpressions ';' optionalexpressions
                  ;

iterationoptions2 : optionalexpressions ';' optionalexpressions ';' optionalexpressions
                  ;

expressions : expr
            | expr ',' expressions
            ;

expr : tINTEGER                { $$ = new cdk::integer_node(LINE, $1); }
	   | tREAL                   { $$ = new cdk::double_node(LINE, $1); }
	   //| tSTRING                 { $$ = new cdk::string_node(LINE, $1); }
     | str                       { $$ = new cdk::string_node(LINE, str_tmp); str_tmp=""; }
     | expr '+' expr	         { $$ = new cdk::add_node(LINE, $1, $3); }
     | expr '-' expr	         { $$ = new cdk::sub_node(LINE, $1, $3); }
     | expr '*' expr	         { $$ = new cdk::mul_node(LINE, $1, $3); }
     | expr '/' expr	         { $$ = new cdk::div_node(LINE, $1, $3); }
     | expr '%' expr	         { $$ = new cdk::mod_node(LINE, $1, $3); }
     | expr '<' expr	         { $$ = new cdk::lt_node(LINE, $1, $3); }
     | expr '>' expr	         { $$ = new cdk::gt_node(LINE, $1, $3); }
     | expr tGE expr	         { $$ = new cdk::ge_node(LINE, $1, $3); }
     | expr tLE expr           { $$ = new cdk::le_node(LINE, $1, $3); }
     | expr tNE expr	         { $$ = new cdk::ne_node(LINE, $1, $3); }
     | expr tEQ expr	         { $$ = new cdk::eq_node(LINE, $1, $3); }
     | expr tAND expr          { $$ = new zu::and_node(LINE, $1, $3); }
     | expr tOR expr           { $$ = new zu::or_node(LINE, $1, $3); }
     | '(' expr ')'            { $$ = $2; }
     | '+' expr %prec tUNARY   { $$ = $2; }
     | '-' expr %prec tUNARY   { $$ = new cdk::neg_node(LINE, $2); }
     | tNO expr                { $$ = $2; }
     //| '@'                     { $$ = new zu::read_node(LINE); }
     //| lval '?'                { $$ = new zu::lvalue_node(LINE, $1); }
     | lval                    { $$ = new zu::rvalue_node(LINE, $1); }  //FIXME
     | lval '=' expr           { $$ = new zu::assignment_node(LINE, $1, $3); }
     //| lval '[' expr ']'       { $$ = new zu::index_node(LINE, $1, $3); }
     //| lval '=' '[' expr ']'   { $$ = new zu::memory_allocation(LINE, $1, $4); }
     ;

lval : tIDENTIFIER             { $$ = new zu::lvalue_node(LINE, $1); }
     ;

str : tSTRING                  { str_tmp = *$1 + str_tmp; }
    | str tSTRING              { str_tmp = *$2 + str_tmp; }
    ;

%%
