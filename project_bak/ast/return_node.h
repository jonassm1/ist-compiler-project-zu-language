// $Id: return_node.h,v 1.1 2016/03/17 23:54:45 ist173891 Exp $
#ifndef __ZU_RETURNNODE_H__
#define __ZU_RETURNNODE_H__

namespace zu {

  /**
   * Class for describing return nodes.
   */
  class return_node: public cdk::basic_node {

  public:
    inline return_node(int lineno) :
        cdk::basic_node(lineno) {
    }

  public:

    void accept(basic_ast_visitor *sp, int level) {
      sp->do_return_node(this, level);
    }

  };

} // zu

#endif
