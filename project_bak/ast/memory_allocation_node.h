#ifndef __ZU_MEMORYALLOCATIONNODE_H__
#define __ZU_MEMORYALLOCATIONNODE_H__

#include <cdk/ast/expression_node.h>
#include <cdk/basic_type.h>

namespace zu {

  /**
   * Class for describing memory allocation declaration
   */
   class memory_allocation_node: public cdk::expression_node {

     cdk::expression_node *_size;

   public:
    inline memory_allocation_node(int lineno, basic_type *type,
                                     cdk::expression_node *size) :
            cdk::expression_node(lineno), _size(size) {
        this->type(type);
    }

   public:

    inline cdk::expression_node *size() {
        return _size;
    }

    void accept(basic_ast_visitor *sp, int level) {
      sp->do_memory_allocation_node(this, level);
    }

   };

} // zu

#endif
