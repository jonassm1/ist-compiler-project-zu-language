#ifndef __ZU_DECLARATIONNODE_H__
#define __ZU_DECLARATIONNODE_H__

#include <string>
#include <cdk/ast/basic_node.h>
#include <cdk/basic_type.h>

namespace zu {

  /**
   * Class for describing declaring variables.
   */
   class declaration_node: public cdk::basic_node {
     // This must be a pointer, so that we can anchor a dynamic
     // object and be able to change/delete it afterwards.
     basic_type *_type;
     std::string *_id;

   public:
    inline declaration_node(int lineno, basic_type *type, std::string *id) :
            cdk::basic_node(lineno), _type(type), _id(id) {
    }

   public:
    inline basic_type *type() {
        return _type;
    }

    inline std::string *id() {
        return _id;
    }

    void accept(basic_ast_visitor *sp, int level) {
      sp->do_declaration_node(this, level);
    }

   };

} // zu

#endif
