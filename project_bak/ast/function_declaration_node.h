#ifndef __ZU_FUNCTIONDECLARATIONNODE_H__
#define __ZU_FUNCTIONDECLARATIONNODE_H__

#include <string>
#include <cdk/ast/basic_node.h>
#include <cdk/ast/sequence_node.h>
#include <cdk/basic_type.h>
#include "ast/lvalue_node.h"

namespace zu {

  /**
   * Class for describing function declaration
   */
   class function_declaration_node: public cdk::basic_node {
     // This must be a pointer, so that we can anchor a dynamic
     // object and be able to change/delete it afterwards.
     basic_type *_type;
     std::string *_id;
     cdk::sequence_node *_args;
     zu::lvalue_node *_lvalue;//opcional

   public:
    inline function_declaration_node(int lineno, basic_type *type, std::string *id,
                                     cdk::sequence_node *_args, zu::lvalue_node *lvalue) :
            cdk::basic_node(lineno), _type(type), _id(id), _lvalue(lvalue) {
    }

   public:
    inline basic_type *type() {
        return _type;
    }

    inline std::string *id() {
        return _id;
    }

    inline cdk::sequence_node *args() {
        return _args;
    }

    inline zu::lvalue_node *lvalue() {
        return _lvalue;
    }

    void accept(basic_ast_visitor *sp, int level) {
      sp->do_function_declaration_node(this, level);
    }

   };

} // zu

#endif
