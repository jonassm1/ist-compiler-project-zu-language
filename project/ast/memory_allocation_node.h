#ifndef __ZU_MEMORYALLOCATIONNODE_H__
#define __ZU_MEMORYALLOCATIONNODE_H__

#include <cdk/ast/expression_node.h>
#include <cdk/basic_type.h>

namespace zu {

  /**
   * Class for describing memory allocation declaration
   */
   class memory_allocation_node: public cdk::expression_node {
     zu::lvalue_node *_lvalue;
     cdk::expression_node *_size;

   public:
    inline memory_allocation_node(int lineno, zu::lvalue_node *lvalue, cdk::expression_node *size) :
            cdk::expression_node(lineno), _lvalue(lvalue), _size(size) {
    }

   public:

    inline zu::lvalue_node *lvalue() {
        return _lvalue;
    }

    inline cdk::expression_node *size() {
        return _size;
    }

    void accept(basic_ast_visitor *sp, int level) {
      sp->do_memory_allocation_node(this, level);
    }

   };

} // zu

#endif
