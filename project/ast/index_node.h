#ifndef __ZU_INDEXNODE_H__
#define __ZU_INDEXNODE_H__

#include <string>
#include <cdk/ast/expression_node.h>

namespace zu {


   class index_node: public zu::lvalue_node {

     cdk::expression_node *_id;
     cdk::expression_node *_pos;

   public:
    inline index_node(int lineno, cdk::expression_node *id, cdk::expression_node *pos) :
            zu::lvalue_node(lineno, ""), _id(id), _pos(pos){
    }

   public:
    inline cdk::expression_node *id() {
        return _id;
    }

    inline cdk::expression_node *pos() {
        return _pos;
    }

    void accept(basic_ast_visitor *sp, int level) {
      sp->do_index_node(this, level);
    }

   };

} // zu

#endif
