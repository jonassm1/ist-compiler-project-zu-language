%{
// $Id: zu_parser.y,v 1.25 2016/05/20 11:13:39 ist173891 Exp $
//-- don't change *any* of these: if you do, you'll break the compiler.
#include <cdk/compiler.h>
#include "ast/all.h"
#define LINE       compiler->scanner()->lineno()
#define yylex()    compiler->scanner()->scan()
#define yyerror(s) compiler->scanner()->error(s)
#define YYPARSE_PARAM_TYPE std::shared_ptr<cdk::compiler>
#define YYPARSE_PARAM      compiler
//-- don't change *any* of these --- END!
%}

%union {
  int32_t               i;	/* integer value */
  double                d;
  std::string          *s;	/* symbol name or string literal */
  cdk::basic_node      *node;	/* node pointer */
  cdk::sequence_node   *sequence;
  cdk::expression_node *expression; /* expression nodes */
  zu::block_node       *block;
  basic_type           *type;
  zu::lvalue_node      *lvalue;
};

%token <i> tINTEGER
%token <s> tIDENTIFIER tSTRING
%token <d> tREAL
%token tNO tAND tOR tCONTINUE tBREAK tRETURN

%right IFX ':' STRX tSTRING

%right '='
%left tOR
%left tAND
%nonassoc tNO
%left tEQ tNE
%left tGE tLE '>' '<'
%left '+' '-'
%left '*' '/' '%'
%left  '$'
%nonassoc UNARY
%nonassoc '(' ')' '[' ']'

%nonassoc '{'

%type <node> file declaration variable instruction function conditional iteration
%type <type> type
%type <sequence> declarations variables expressions instructions optionalexpressions
%type <expression> expr literal function_call
%type <block> block
%type <lvalue> lval
%type <s> str markers

%start file

%{
//-- The rules below will be included in yyparse, the main parsing function.
#include <string>
std::string str_tmp;
%}
%%

file: /* empty file */   { /*FIXME Segmentation fault caso o ficheiro esteja vazio*/ }
      | declarations     { compiler->ast(new cdk::sequence_node(LINE, $1)); }
      ;

declarations : declaration                { $$ = new cdk::sequence_node(LINE, $1); }
             | declarations declaration   { $$ = new cdk::sequence_node(LINE, $2, $1); }
           //|                            /* empty file */
             ;

declaration : variable ';'   {$$ = $1;}
            | function       {$$ = $1;}
            ;

function : type tIDENTIFIER markers '(' variables ')' '=' literal       { $$ = new zu::function_declaration_node(LINE, $1, $2, $5, $8); }
         | type tIDENTIFIER markers '(' ')' '=' literal                 { $$ = new zu::function_declaration_node(LINE, $1, $2, nullptr, $7); }
         | type tIDENTIFIER '(' variables ')' '=' literal               { $$ = new zu::function_declaration_node(LINE, $1, $2, $4, $7); }
         | type tIDENTIFIER '(' ')' '=' literal                         { $$ = new zu::function_declaration_node(LINE, $1, $2, nullptr, $6); }
         | type tIDENTIFIER markers '(' variables ')'                   { $$ = new zu::function_declaration_node(LINE, $1, $2, $5, nullptr); }
         | type tIDENTIFIER markers '(' ')'                             { $$ = new zu::function_declaration_node(LINE, $1, $2, nullptr, nullptr); }
         | type tIDENTIFIER '(' variables ')'                           { $$ = new zu::function_declaration_node(LINE, $1, $2, $4, nullptr); }
         | type tIDENTIFIER '(' ')'                                     { $$ = new zu::function_declaration_node(LINE, $1, $2, nullptr, nullptr); }
         | type tIDENTIFIER markers '(' variables ')' '=' literal block {   if(*$3=="?") yyerror( std::string("Line: " + std::to_string(LINE) + ": Error: Definition of function cannot have the import qualifier.").c_str() );
                                                                            $$ = new zu::function_definition_node(LINE, $1, $2, $5, $8, $9);
                                                                        }
         | type tIDENTIFIER markers '(' ')' '=' literal block           {   if(*$3=="?") yyerror( std::string("Line: " + std::to_string(LINE) + ": Error: Definition of function cannot have the import qualifier.").c_str() );
                                                                            $$ = new zu::function_definition_node(LINE, $1, $2, nullptr, $7, $8);
                                                                        }
         | type tIDENTIFIER '(' variables ')' '=' literal block         { $$ = new zu::function_definition_node(LINE, $1, $2, $4, $7, $8); }
         | type tIDENTIFIER '(' ')' '=' literal block                   { $$ = new zu::function_definition_node(LINE, $1, $2, nullptr, $6, $7); }
         | type tIDENTIFIER markers '(' variables ')' block             {   if(*$3=="?") yyerror( std::string("Line: " + std::to_string(LINE) + ": Error: Definition of function cannot have the import qualifier.").c_str() );
                                                                            $$ = new zu::function_definition_node(LINE, $1, $2, $5, nullptr, $7);
                                                                        }
         | type tIDENTIFIER markers '(' ')' block                       {   if(*$3=="?") yyerror( std::string("Line: " + std::to_string(LINE) + ": Error: Definition of function cannot have the import qualifier.").c_str() );
                                                                            $$ = new zu::function_definition_node(LINE, $1, $2, nullptr, nullptr, $6);
                                                                        }
         | type tIDENTIFIER '(' variables ')' block                     { $$ = new zu::function_definition_node(LINE, $1, $2, $4, nullptr, $6); }
         | type tIDENTIFIER '(' ')' block                               { $$ = new zu::function_definition_node(LINE, $1, $2, nullptr, nullptr, $5); }
         ;

type : '#'             { $$ = new basic_type(4, basic_type::TYPE_INT); }
     | '%'             { $$ = new basic_type(8, basic_type::TYPE_DOUBLE); }
     | '$'             { $$ = new basic_type(4, basic_type::TYPE_STRING); }
     | '!'             { $$ = new basic_type(); }
     | '<' type '>'    { $$ = new basic_type(4, basic_type::TYPE_POINTER); }
     ;

markers : '!'   { $$ = new std::string("!"); }
        | '?'   { $$ = new std::string("?"); }
        ;

block : '{' '}'                              {$$ = new zu::block_node(LINE, nullptr, nullptr);}
      | '{' declarations instructions '}'    {$$ = new zu::block_node(LINE, $2, $3);}
      | '{' declarations '}'                 {$$ = new zu::block_node(LINE, $2, nullptr);}
      | '{' instructions '}'                 {$$ = new zu::block_node(LINE, nullptr, $2);}
      ;

instructions : instruction                { $$ = new cdk::sequence_node(LINE, $1); }
             | instructions instruction   { $$ = new cdk::sequence_node(LINE, $2, $1); }
      //     |                            /* empty */
             ;

instruction : expr ';'               { $$ = new zu::evaluation_node(LINE, $1); }
            | expr '!'               { $$ = new zu::print_node(LINE, $1); }
            | expr '!' '!'           { $$ = new zu::print_node(LINE, $1, true); }
            | tBREAK                 { $$ = new zu::break_node(LINE); }
            | tCONTINUE              { $$ = new zu::continue_node(LINE); }
            | tRETURN                { $$ = new zu::return_node(LINE); }
            | conditional            { $$ = $1; }
            | iteration              { $$ = $1; }
            | block                  { $$ = $1; }
            ;

conditional : '[' expr ']' '#' instruction                  { $$ = new zu::if_node(LINE, $2, $5); }
            | '[' expr ']' '?' instruction %prec IFX        { $$ = new zu::if_node(LINE, $2, $5); }
            | '[' expr ']' '?' instruction ':' instruction  { $$ = new zu::if_else_node(LINE, $2, $5, $7); }
            ;

iteration : '[' variables ';' optionalexpressions ';' optionalexpressions ']' instruction             { $$ = new zu::for_node(LINE, $2, $4, $6, $8); }
          | '[' optionalexpressions ';' optionalexpressions ';' optionalexpressions ']' instruction   { $$ = new zu::for_node(LINE, $2, $4, $6, $8); }
          ;

optionalexpressions :                 { $$ = nullptr; }
                    | expressions     { $$ = $1; }
                    ;

expressions : expr                    { $$ = new cdk::sequence_node(LINE, $1); }
            | expr ',' expressions    { $$ = new cdk::sequence_node(LINE, $1, $3); }
            ;

variable : type tIDENTIFIER                    {
                                                  if($1->size()==0) {
                                                    yyerror( std::string("Line: " + std::to_string(LINE) + ": Error: Variable cannot be void.").c_str() );
                                                  }
                                                  $$ = new zu::declaration_node(LINE, $1, $2);
                                               }
         | type tIDENTIFIER markers            {
                                                  if($1->size()==0) {
                                                    yyerror( std::string("Line: " + std::to_string(LINE) + ": Error: Variable cannot be void.").c_str() );
                                                  }
                                                  $$ = new zu::declaration_node(LINE, $1, $2);
                                               }
         | type tIDENTIFIER markers '=' expr   {
                                                  if($1->size()==0) {
                                                    yyerror( std::string("Line: " + std::to_string(LINE) + ": Error: Variable cannot be void.").c_str() );
                                                  }
                                                  $$ = new zu::definition_node(LINE, $1, $2, $5);
                                               }
         | type tIDENTIFIER '=' expr           {
                                                  if($1->size()==0) {
                                                    yyerror( std::string("Line: " + std::to_string(LINE) + ": Error: Variable cannot be void.").c_str() );
                                                  }
                                                  $$ = new zu::definition_node(LINE, $1, $2, $4);
                                               }
         ;

variables : variable                { $$ = new cdk::sequence_node(LINE, $1); }
          | variable ',' variables  { $$ = new cdk::sequence_node(LINE, $1, $3); }
          ;

literal : tINTEGER             { $$ = new cdk::integer_node(LINE, $1); }
        | tREAL                { $$ = new cdk::double_node(LINE, $1); }
        | str %prec STRX       { $$ = new cdk::string_node(LINE, str_tmp); str_tmp=""; }
        //| error                { yyerror( std::string("Line: " + std::to_string(LINE) + ": Error: Literal type not recognized.").c_str() ); yyerrok;}
        ;

str : tSTRING                  { str_tmp = *$1 + str_tmp; }
    | str tSTRING              { str_tmp = *$2 + str_tmp; }
    ;

expr : literal                    { $$ = $1; }
     | expr '+' expr              { $$ = new cdk::add_node(LINE, $1, $3); }
     | expr '-' expr	          { $$ = new cdk::sub_node(LINE, $1, $3); }
     | expr '*' expr	          { $$ = new cdk::mul_node(LINE, $1, $3); }
     | expr '/' expr	          { $$ = new cdk::div_node(LINE, $1, $3); }
     | expr '%' expr	          { $$ = new cdk::mod_node(LINE, $1, $3); }
     | expr '<' expr	          { $$ = new cdk::lt_node(LINE, $1, $3); }
     | expr '>' expr	          { $$ = new cdk::gt_node(LINE, $1, $3); }
     | expr tGE expr              { $$ = new cdk::ge_node(LINE, $1, $3); }
     | expr tLE expr              { $$ = new cdk::le_node(LINE, $1, $3); }
     | expr tNE expr	          { $$ = new cdk::ne_node(LINE, $1, $3); }
     | expr tEQ expr	          { $$ = new cdk::eq_node(LINE, $1, $3); }
     | expr tAND expr             { $$ = new zu::and_node(LINE, $1, $3); }
     | expr tOR expr              { $$ = new zu::or_node(LINE, $1, $3); }
     | '(' expr ')'               { $$ = $2; }
     | '+' expr %prec UNARY       { $$ = $2; }
     | '-' expr %prec UNARY       { $$ = new cdk::neg_node(LINE, $2); }
     | tNO expr                   { $$ = new zu::not_node(LINE, $2); }
     | '@'                        { $$ = new zu::read_node(LINE); }
     | lval '?'                   { $$ = new zu::address_node(LINE, $1); }
     | lval                       { $$ = $1; }
     | lval '=' expr              { $$ = new zu::assignment_node(LINE, $1, $3); }
     | lval '=' '[' expr ']'      { $$ = new zu::memory_allocation_node(LINE, $1, $4); }
     | function_call              { $$ = $1; }
     ;

function_call : tIDENTIFIER '(' expressions ')'   { $$ = new zu::function_call_node(LINE, $1, $3); }
              | tIDENTIFIER '(' ')'               { $$ = new zu::function_call_node(LINE, $1, nullptr); }

lval : tIDENTIFIER                       { $$ = new zu::lvalue_node(LINE, $1); }
     | expr '[' expr ']'                 { $$ = new zu::index_node(LINE, $1, $3); }
     ;


%%
