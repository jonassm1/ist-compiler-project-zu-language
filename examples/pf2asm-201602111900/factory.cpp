// $Id: factory.cpp,v 1.1 2014/02/26 18:55:25 david Exp $ -*- c++ -*-

#include "factory.h"

/**
 * This object is automatically registered by the constructor in the
 * superclass' language registry.
 */
pf2asm::factory pf2asm::factory::_self;
