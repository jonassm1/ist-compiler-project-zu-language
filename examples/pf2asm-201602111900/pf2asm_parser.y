%{
// $Id: $
//-- don't change *any* of these: if you do, you'll break the compiler.
#include <cdk/compiler.h>
#include "ast/all.h"
#define LINE       compiler->scanner()->lineno()
#define yylex()    compiler->scanner()->scan()
#define yyerror(s) compiler->scanner()->error(s)
#define YYPARSE_PARAM_TYPE std::shared_ptr<cdk::compiler>
#define YYPARSE_PARAM      compiler
//-- don't change *any* of these --- END!
%}

%union {
  int                  i;             /* integer number */
  double               d;             /* real number */
  std::string         *s;             /* symbol name or string literal */
  cdk::basic_node     *node;          /* node pointer */
  cdk::sequence_node  *sequence;      /* node pointer */
}

%token tNOP tINT tADD tSUB tMUL tDIV tMOD tNEG tINCR tDECR tGT tGE tLT tLE
%token tEQ tNE tAND tOR tXOR tNOT tROTL tROTR tSHTL tSHTRU tSHTRS tLOCAL
%token tADDR tLOCV tADDRV tLOCA tADDRA tLOAD tSTORE tLDCHR tSTCHR tLD16
%token tST16 tENTER tSTART tALLOC tLEAVE tTRASH tCALL tRET tRETN tBRANCH
%token tLEAP tJMP tJZ tJNZ tDUP tDDUP tSWAP tSP tPUSH tPOP tI2D tF2D tD2I tD2F
%token tDADD tDSUB tDMUL tDDIV tDCMP tDNEG tDLOAD tDSTORE tDPUSH tDPOP
%token tNIL tTEXT tRODATA tDATA tBSS tALIGN tEXTERN tCOMMON tGLOBAL tLABEL
%token tCONST tSTR tCHAR tID tBYTE tFLOAT tDOUBLE tULDCHR tULD16
%token tUDIV tUMOD tUGT tUGE tULT tULE tJEQ tJNE tJGT tJGE tJLT tJLE tJUGT
%token tJUGE tJULT tJULE

%token T_AND T_OR T_NE T_LE T_GE

%token<i> T_LIT_INT
%token<d> T_LIT_REAL
%token<s> T_LIT_STRING T_ID

%type<node> instruction data function addressing loadstore jumps
%type<node> bitwise logical arithmetic relational
%type<sequence> file instructions  
%type<s> label type
%type<i> integer bytes offset

%nonassoc '?'
%nonassoc ':'
%left T_OR
%left T_AND
%left T_NE T_EQ
%left '<' T_LE T_GE '>'
%left '+' '-'
%left '*' '/' '%'
%right T_UMINUS '!'

%%

file	          : /* empty */  { compiler->ast($$ = new cdk::sequence_node(LINE, new cdk::nil_node(LINE))); }
                | instructions { compiler->ast($$ = $1); }
                ;

instructions    : instruction                                { $$ = new cdk::sequence_node(LINE, $1);     }
                | instructions instruction                   { $$ = new cdk::sequence_node(LINE, $2, $1); }
                ;

instruction     : tNOP              { $$ = new pf2asm::NOP(LINE); }
                | tINCR integer     { $$ = new pf2asm::INCR(LINE, $2); }
                | tDECR integer     { $$ = new pf2asm::DECR(LINE, $2); }
                | tALLOC            { $$ = new pf2asm::ALLOC(LINE); }
                | tTRASH bytes      { $$ = new pf2asm::TRASH(LINE, $2); }
                | tDUP              { $$ = new pf2asm::DUP(LINE); }
                | tDDUP             { $$ = new pf2asm::DDUP(LINE); }
                | tSWAP             { $$ = new pf2asm::SWAP(LINE); }
                | tSP               { $$ = new pf2asm::SP(LINE); }
                | tI2D              { $$ = new pf2asm::I2D(LINE); }
                | tF2D              { $$ = new pf2asm::F2D(LINE); }
                | tD2I              { $$ = new pf2asm::D2I(LINE); }
                | tD2F              { $$ = new pf2asm::D2F(LINE); }
                | tNIL              { $$ = new pf2asm::NIL(LINE); }
                | data              { $$ = $1; }
                | function          { $$ = $1; }
                | addressing        { $$ = $1; }
                | loadstore         { $$ = $1; }
                | bitwise           { $$ = $1; }
                | logical           { $$ = $1; }
                | arithmetic        { $$ = $1; }
                | relational        { $$ = $1; }
                | jumps             { $$ = $1; }
                ;

bytes           : integer           { $$ = $1; };

data            : tCONST   integer      { $$ = new pf2asm::data::CONST  (LINE, $2); }
                | tSTR     T_LIT_STRING { $$ = new pf2asm::data::STR    (LINE, *$2); delete $2; }
                | tCHAR    T_LIT_STRING { $$ = new pf2asm::data::CHAR   (LINE, *$2); delete $2; }
                | tID      label        { $$ = new pf2asm::data::ID     (LINE, *$2); delete $2; }
                | tBYTE    integer      { $$ = new pf2asm::data::BYTE   (LINE, $2); }
                | tINT     integer      { $$ = new pf2asm::data::INT    (LINE, $2); }
                | tFLOAT   T_LIT_REAL   { $$ = new pf2asm::data::FLOAT  (LINE, $2); }
                | tDOUBLE  T_LIT_REAL   { $$ = new pf2asm::data::DOUBLE (LINE, $2); }
                ;

addressing      : tTEXT             { $$ = new pf2asm::addressing::TEXT   (LINE); }
                | tRODATA           { $$ = new pf2asm::addressing::RODATA (LINE); }
                | tDATA             { $$ = new pf2asm::addressing::DATA   (LINE); }
                | tBSS              { $$ = new pf2asm::addressing::BSS    (LINE); }
                | tALIGN            { $$ = new pf2asm::addressing::ALIGN  (LINE); }
                | tCOMMON integer   { $$ = new pf2asm::addressing::COMMON (LINE, $2); }
                | tLOCAL  offset    { $$ = new pf2asm::addressing::LOCAL  (LINE, $2); }
                | tLOCV   offset    { $$ = new pf2asm::addressing::LOCV   (LINE, $2); }
                | tLOCA   offset    { $$ = new pf2asm::addressing::LOCA   (LINE, $2); }
                | tGLOBAL label ',' type  { $$ = new pf2asm::addressing::GLOBAL(LINE, *$2, *$4); delete $2; delete $4; }
                | tEXTERN label     { $$ = new pf2asm::addressing::EXTERN (LINE, *$2); delete $2; }
                | tLABEL  label     { $$ = new pf2asm::addressing::LABEL  (LINE, *$2); delete $2; }
                | tADDR   label     { $$ = new pf2asm::addressing::ADDR   (LINE, *$2); delete $2; }
                | tADDRV  label     { $$ = new pf2asm::addressing::ADDRV  (LINE, *$2); delete $2; }
                | tADDRA  label     { $$ = new pf2asm::addressing::ADDRA  (LINE, *$2); delete $2; }
                ;

offset          : integer         { $$ = $1; };
label           : T_ID            { $$ = $1; };
type            : T_ID            { $$ = $1; };

function        : tENTER bytes   { $$ = new pf2asm::function::ENTER(LINE, $2); }
                | tSTART         { $$ = new pf2asm::function::START(LINE);   }
                | tLEAVE         { $$ = new pf2asm::function::LEAVE(LINE);   }
                | tCALL  label   { $$ = new pf2asm::function::CALL(LINE, *$2); delete $2; }
                | tRET           { $$ = new pf2asm::function::RET(LINE);     }
                | tRETN  bytes   { $$ = new pf2asm::function::RETN(LINE, $2);  }
                | tPUSH          { $$ = new pf2asm::function::PUSH(LINE);    }
                | tPOP           { $$ = new pf2asm::function::POP(LINE);     }
                | tDPUSH         { $$ = new pf2asm::function::DPUSH(LINE);   }
                | tDPOP          { $$ = new pf2asm::function::DPOP(LINE);    }
                ;

loadstore       : tULDCHR        { $$ = new pf2asm::loadstore::ULDCHR(LINE); }
                | tULD16         { $$ = new pf2asm::loadstore::ULD16(LINE);  }
                | tLOAD          { $$ = new pf2asm::loadstore::LOAD(LINE);   }
                | tSTORE         { $$ = new pf2asm::loadstore::STORE(LINE);  }
                | tLDCHR         { $$ = new pf2asm::loadstore::LDCHR(LINE);  }
                | tSTCHR         { $$ = new pf2asm::loadstore::STCHR(LINE);  }
                | tLD16          { $$ = new pf2asm::loadstore::LD16(LINE);   }
                | tST16          { $$ = new pf2asm::loadstore::ST16(LINE);   }
                | tDLOAD         { $$ = new pf2asm::loadstore::DLOAD(LINE);  }
                | tDSTORE        { $$ = new pf2asm::loadstore::DSTORE(LINE); }
                ;

arithmetic      : tADD           { $$ = new pf2asm::arithmetic::ADD(LINE);  }
                | tSUB           { $$ = new pf2asm::arithmetic::SUB(LINE);  }
                | tMUL           { $$ = new pf2asm::arithmetic::MUL(LINE);  }
                | tDIV           { $$ = new pf2asm::arithmetic::DIV(LINE);  }
                | tMOD           { $$ = new pf2asm::arithmetic::MOD(LINE);  }
                | tNEG           { $$ = new pf2asm::arithmetic::NEG(LINE);  }
                | tUDIV          { $$ = new pf2asm::arithmetic::UDIV(LINE); }
                | tUMOD          { $$ = new pf2asm::arithmetic::UMOD(LINE); }
                | tDADD          { $$ = new pf2asm::arithmetic::DADD(LINE); }
                | tDSUB          { $$ = new pf2asm::arithmetic::DSUB(LINE); }
                | tDMUL          { $$ = new pf2asm::arithmetic::DMUL(LINE); }
                | tDDIV          { $$ = new pf2asm::arithmetic::DDIV(LINE); }
                | tDNEG          { $$ = new pf2asm::arithmetic::DNEG(LINE); }
                ;
                
bitwise         : tROTL          { $$ = new pf2asm::bitwise::ROTL(LINE);  }
                | tROTR          { $$ = new pf2asm::bitwise::ROTR(LINE);  }
                | tSHTL          { $$ = new pf2asm::bitwise::SHTL(LINE);  }
                | tSHTRU         { $$ = new pf2asm::bitwise::SHTRU(LINE); }
                | tSHTRS         { $$ = new pf2asm::bitwise::SHTRS(LINE); }
                ;
                
logical         : tAND           { $$ = new pf2asm::logical::AND(LINE); }
                | tOR            { $$ = new pf2asm::logical::OR(LINE);  }
                | tXOR           { $$ = new pf2asm::logical::XOR(LINE); }
                | tNOT           { $$ = new pf2asm::logical::NOT(LINE); }
                ;

relational      : tGT            { $$ = new pf2asm::relational::GT(LINE);   }
                | tGE            { $$ = new pf2asm::relational::GE(LINE);   }
                | tLT            { $$ = new pf2asm::relational::LT(LINE);   }
                | tLE            { $$ = new pf2asm::relational::LE(LINE);   }
                | tEQ            { $$ = new pf2asm::relational::EQ(LINE);   }
                | tNE            { $$ = new pf2asm::relational::NE(LINE);   }
                | tUGT           { $$ = new pf2asm::relational::UGT(LINE);  }
                | tUGE           { $$ = new pf2asm::relational::UGE(LINE);  }
                | tULT           { $$ = new pf2asm::relational::ULT(LINE);  }
                | tULE           { $$ = new pf2asm::relational::ULE(LINE);  }
                | tDCMP          { $$ = new pf2asm::relational::DCMP(LINE); }
                ;

jumps           : tBRANCH           { $$ = new pf2asm::jumps::BRANCH (LINE); }
                | tLEAP             { $$ = new pf2asm::jumps::LEAP   (LINE); }
                | tJMP  label       { $$ = new pf2asm::jumps::JMP    (LINE, *$2); delete $2; }
                | tJZ   label       { $$ = new pf2asm::jumps::JZ     (LINE, *$2); delete $2; }
                | tJNZ  label       { $$ = new pf2asm::jumps::JNZ    (LINE, *$2); delete $2; }
                | tJEQ  label       { $$ = new pf2asm::jumps::JEQ    (LINE, *$2); delete $2; }
                | tJNE  label       { $$ = new pf2asm::jumps::JNE    (LINE, *$2); delete $2; }
                | tJGT  label       { $$ = new pf2asm::jumps::JGT    (LINE, *$2); delete $2; }
                | tJGE  label       { $$ = new pf2asm::jumps::JGE    (LINE, *$2); delete $2; }
                | tJLT  label       { $$ = new pf2asm::jumps::JLT    (LINE, *$2); delete $2; }
                | tJLE  label       { $$ = new pf2asm::jumps::JLE    (LINE, *$2); delete $2; }
                | tJUGT label       { $$ = new pf2asm::jumps::JUGT   (LINE, *$2); delete $2; }
                | tJUGE label       { $$ = new pf2asm::jumps::JUGE   (LINE, *$2); delete $2; }
                | tJULT label       { $$ = new pf2asm::jumps::JULT   (LINE, *$2); delete $2; }
                | tJULE label       { $$ = new pf2asm::jumps::JULE   (LINE, *$2); delete $2; }
                ;

integer         : T_LIT_INT                       { $$ = $1; }
                | integer  '+'  integer           { $$ = $1 +  $3; }
                | integer  '-'  integer           { $$ = $1 -  $3; }
                | integer  '*'  integer           { $$ = $1 *  $3; }
                | integer  '/'  integer           { $$ = $1 /  $3; }
                | integer  '%'  integer           { $$ = $1 %  $3; }
                | integer  '<'  integer           { $$ = $1 <  $3; }
                | integer T_LE  integer           { $$ = $1 <= $3; }
                | integer T_EQ  integer           { $$ = $1 == $3; }
                | integer T_GE  integer           { $$ = $1 >= $3; }
                | integer  '>'  integer           { $$ = $1 >  $3; }
                | integer T_NE  integer           { $$ = $1 != $3; }
                | '!' integer                     { $$ = !$2; }
                | '-' integer %prec T_UMINUS      { $$ = -$2; }
                | '+' integer %prec T_UMINUS      { $$ =  $2; }
                | '(' integer ')'                 { $$ =  $2; }
                | integer '?' integer ':' integer { $$ = $1 ? $3 : $5; }
                ;

%%
