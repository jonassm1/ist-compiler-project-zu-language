// $Id: ROTR.h,v 1.1 2014/02/26 18:55:34 david Exp $
#ifndef __PF2ASM_NODE_bitwise_ROTR_H__
#define __PF2ASM_NODE_bitwise_ROTR_H__

#include <cdk/ast/basic_node.h>
#include "targets/basic_ast_visitor.h"

namespace pf2asm {
  namespace bitwise {

    class ROTR: public cdk::basic_node {
    public:
      inline ROTR(int lineno) :
          cdk::basic_node(lineno) {
      }
      inline void accept(basic_ast_visitor *sp, int level) {
        sp->do_ROTR(this, level);
      }
    };

  } // bitwise
} // pf2asm

#endif
