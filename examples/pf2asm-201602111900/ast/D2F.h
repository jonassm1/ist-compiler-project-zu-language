// $Id: D2F.h,v 1.1 2014/02/26 18:55:26 david Exp $
#ifndef __PF2ASM_NODE_nodes_D2F_H__
#define __PF2ASM_NODE_nodes_D2F_H__

#include <cdk/ast/basic_node.h>
#include "targets/basic_ast_visitor.h"

namespace pf2asm {

  class D2F: public cdk::basic_node {
  public:
    inline D2F(int lineno) :
        cdk::basic_node(lineno) {
    }
    inline void accept(basic_ast_visitor *sp, int level) {
      sp->do_D2F(this, level);
    }
  };

} // pf2asm

#endif
