// $Id: COMMON.h,v 1.1 2014/02/26 18:55:28 david Exp $
#ifndef __PF2ASM_NODE_addressing_COMMON_H__
#define __PF2ASM_NODE_addressing_COMMON_H__

#include <cdk/ast/basic_node.h>
#include "targets/basic_ast_visitor.h"

namespace pf2asm {
  namespace addressing {

    class COMMON: public cdk::basic_node {
      int _value;

    public:
      inline COMMON(int lineno, int value) :
          cdk::basic_node(lineno), _value(value) {
      }
      inline int value() const {
        return _value;
      }
      inline void accept(basic_ast_visitor *sp, int level) {
        sp->do_COMMON(this, level);
      }
    };

  } // addressing
} // pf2asm

#endif
