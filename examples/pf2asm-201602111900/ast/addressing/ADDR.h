// $Id: ADDR.h,v 1.1 2014/02/26 18:55:28 david Exp $
#ifndef __PF2ASM_NODE_addressing_ADDR_H__
#define __PF2ASM_NODE_addressing_ADDR_H__

#include <cdk/ast/basic_node.h>
#include "targets/basic_ast_visitor.h"

namespace pf2asm {
  namespace addressing {

    class ADDR: public cdk::basic_node {
      std::string _label;

    public:
      inline ADDR(int lineno, std::string &label) :
          cdk::basic_node(lineno), _label(label) {
      }
      inline const std::string &label() const {
        return _label;
      }
      inline void accept(basic_ast_visitor *sp, int level) {
        sp->do_ADDR(this, level);
      }
    };

  } // addressing
} // pf2asm

#endif
