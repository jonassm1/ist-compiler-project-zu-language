// $Id: GLOBAL.h,v 1.1 2014/02/26 18:55:28 david Exp $
#ifndef __PF2ASM_NODE_addressing_GLOBAL_H__
#define __PF2ASM_NODE_addressing_GLOBAL_H__

#include <cdk/ast/basic_node.h>
#include "targets/basic_ast_visitor.h"

namespace pf2asm {
  namespace addressing {

    class GLOBAL: public cdk::basic_node {
      std::string _label;
      std::string _type;

    public:
      inline GLOBAL(int lineno, std::string &label, std::string &type) :
          cdk::basic_node(lineno), _label(label), _type(type) {
      }
      inline const std::string &label() const {
        return _label;
      }
      inline const std::string &type() const {
        return _type;
      }
      inline void accept(basic_ast_visitor *sp, int level) {
        sp->do_GLOBAL(this, level);
      }
    };

  } // addressing
} // pf2asm

#endif
