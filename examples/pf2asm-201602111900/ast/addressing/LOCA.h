// $Id: LOCA.h,v 1.1 2014/02/26 18:55:27 david Exp $
#ifndef __PF2ASM_NODE_addressing_LOCA_H__
#define __PF2ASM_NODE_addressing_LOCA_H__

#include <cdk/ast/basic_node.h>
#include "targets/basic_ast_visitor.h"

namespace pf2asm {
  namespace addressing {

    class LOCA: public cdk::basic_node {
      int _offset;

    public:
      inline LOCA(int lineno, int offset) :
          cdk::basic_node(lineno), _offset(offset) {
      }
      inline int offset() const {
        return _offset;
      }
      inline void accept(basic_ast_visitor *sp, int level) {
        sp->do_LOCA(this, level);
      }
    };

  } // addressing
} // pf2asm

#endif
