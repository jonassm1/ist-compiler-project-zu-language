// $Id: BSS.h,v 1.1 2014/02/26 18:55:27 david Exp $
#ifndef __PF2ASM_NODE_addressing_BSS_H__
#define __PF2ASM_NODE_addressing_BSS_H__

#include <cdk/ast/basic_node.h>
#include "targets/basic_ast_visitor.h"

namespace pf2asm {
  namespace addressing {

    class BSS: public cdk::basic_node {
    public:
      inline BSS(int lineno) :
          cdk::basic_node(lineno) {
      }
      inline void accept(basic_ast_visitor *sp, int level) {
        sp->do_BSS(this, level);
      }
    };

  } // addressing
} // pf2asm

#endif
