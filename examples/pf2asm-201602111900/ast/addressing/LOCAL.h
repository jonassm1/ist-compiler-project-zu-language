// $Id: LOCAL.h,v 1.1 2014/02/26 18:55:28 david Exp $
#ifndef __PF2ASM_NODE_addressing_LOCAL_H__
#define __PF2ASM_NODE_addressing_LOCAL_H__

#include <cdk/ast/basic_node.h>
#include "targets/basic_ast_visitor.h"

namespace pf2asm {
  namespace addressing {

    class LOCAL: public cdk::basic_node {
      int _offset;

    public:
      inline LOCAL(int lineno, int offset) :
          cdk::basic_node(lineno), _offset(offset) {
      }
      inline int offset() const {
        return _offset;
      }
      inline void accept(basic_ast_visitor *sp, int level) {
        sp->do_LOCAL(this, level);
      }
    };

  } // addressing
} // pf2asm

#endif
