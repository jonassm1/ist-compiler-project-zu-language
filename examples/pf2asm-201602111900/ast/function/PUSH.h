// $Id: PUSH.h,v 1.1 2014/02/26 18:55:14 david Exp $
#ifndef __PF2ASM_NODE_function_PUSH_H__
#define __PF2ASM_NODE_function_PUSH_H__

#include <cdk/ast/basic_node.h>
#include "targets/basic_ast_visitor.h"

namespace pf2asm {
  namespace function {

    class PUSH: public cdk::basic_node {
    public:
      inline PUSH(int lineno) :
          cdk::basic_node(lineno) {
      }
      inline void accept(basic_ast_visitor *sp, int level) {
        sp->do_PUSH(this, level);
      }
    };

  } // function
} // pf2asm

#endif
