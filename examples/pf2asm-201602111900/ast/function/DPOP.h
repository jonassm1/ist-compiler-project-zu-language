// $Id: DPOP.h,v 1.1 2014/02/26 18:55:14 david Exp $
#ifndef __PF2ASM_NODE_function_DPOP_H__
#define __PF2ASM_NODE_function_DPOP_H__

#include <cdk/ast/basic_node.h>
#include "targets/basic_ast_visitor.h"

namespace pf2asm {
  namespace function {

    class DPOP: public cdk::basic_node {
    public:
      inline DPOP(int lineno) :
          cdk::basic_node(lineno) {
      }
      inline void accept(basic_ast_visitor *sp, int level) {
        sp->do_DPOP(this, level);
      }
    };

  } // function
} // pf2asm

#endif
