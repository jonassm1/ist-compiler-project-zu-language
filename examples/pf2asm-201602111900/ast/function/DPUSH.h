// $Id: DPUSH.h,v 1.1 2014/02/26 18:55:15 david Exp $
#ifndef __PF2ASM_NODE_function_DPUSH_H__
#define __PF2ASM_NODE_function_DPUSH_H__

#include <cdk/ast/basic_node.h>
#include "targets/basic_ast_visitor.h"

namespace pf2asm {
  namespace function {

    class DPUSH: public cdk::basic_node {
    public:
      inline DPUSH(int lineno) :
          cdk::basic_node(lineno) {
      }
      inline void accept(basic_ast_visitor *sp, int level) {
        sp->do_DPUSH(this, level);
      }
    };

  } // function
} // pf2asm

#endif
