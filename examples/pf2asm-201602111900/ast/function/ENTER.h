// $Id: ENTER.h,v 1.1 2014/02/26 18:55:14 david Exp $
#ifndef __PF2ASM_NODE_function_ENTER_H__
#define __PF2ASM_NODE_function_ENTER_H__

#include <cdk/ast/basic_node.h>
#include "targets/basic_ast_visitor.h"

namespace pf2asm {
  namespace function {

    class ENTER: public cdk::basic_node {
      int _value;

    public:
      inline ENTER(int lineno, int value) :
          cdk::basic_node(lineno), _value(value) {
      }
      inline int value() const {
        return _value;
      }
      inline void accept(basic_ast_visitor *sp, int level) {
        sp->do_ENTER(this, level);
      }
    };

  } // function
} // pf2asm

#endif
