// $Id: MUL.h,v 1.1 2014/02/26 18:55:17 david Exp $
#ifndef __PF2ASM_NODE_arithmetic_MUL_H__
#define __PF2ASM_NODE_arithmetic_MUL_H__

#include <cdk/ast/basic_node.h>
#include "targets/basic_ast_visitor.h"

namespace pf2asm {
  namespace arithmetic {

    class MUL: public cdk::basic_node {
    public:
      inline MUL(int lineno) :
          cdk::basic_node(lineno) {
      }
      inline void accept(basic_ast_visitor *sp, int level) {
        sp->do_MUL(this, level);
      }
    };

  } // arithmetic
} // pf2asm

#endif
