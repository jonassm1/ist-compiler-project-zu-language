// $Id: NEG.h,v 1.1 2014/02/26 18:55:16 david Exp $
#ifndef __PF2ASM_NODE_arithmetic_NEG_H__
#define __PF2ASM_NODE_arithmetic_NEG_H__

#include <cdk/ast/basic_node.h>
#include "targets/basic_ast_visitor.h"

namespace pf2asm {
  namespace arithmetic {

    class NEG: public cdk::basic_node {
    public:
      inline NEG(int lineno) :
          cdk::basic_node(lineno) {
      }
      inline void accept(basic_ast_visitor *sp, int level) {
        sp->do_NEG(this, level);
      }
    };

  } // arithmetic
} // pf2asm

#endif
