// $Id: BRANCH.h,v 1.1 2014/02/26 18:55:20 david Exp $
#ifndef __PF2ASM_NODE_jumps_BRANCH_H__
#define __PF2ASM_NODE_jumps_BRANCH_H__

#include <cdk/ast/basic_node.h>
#include "targets/basic_ast_visitor.h"

namespace pf2asm {
  namespace jumps {

    class BRANCH: public cdk::basic_node {
    public:
      inline BRANCH(int lineno) :
          cdk::basic_node(lineno) {
      }
      inline void accept(basic_ast_visitor *sp, int level) {
        sp->do_BRANCH(this, level);
      }
    };

  } // jumps
} // pf2asm

#endif
