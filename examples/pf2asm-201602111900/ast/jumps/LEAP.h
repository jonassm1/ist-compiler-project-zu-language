// $Id: LEAP.h,v 1.1 2014/02/26 18:55:21 david Exp $
#ifndef __PF2ASM_NODE_jumps_LEAP_H__
#define __PF2ASM_NODE_jumps_LEAP_H__

#include <cdk/ast/basic_node.h>
#include "targets/basic_ast_visitor.h"

namespace pf2asm {
  namespace jumps {

    class LEAP: public cdk::basic_node {
    public:
      inline LEAP(int lineno) :
          cdk::basic_node(lineno) {
      }
      inline void accept(basic_ast_visitor *sp, int level) {
        sp->do_LEAP(this, level);
      }
    };

  } // jumps
} // pf2asm

#endif
