// $Id: XOR.h,v 1.1 2014/02/26 18:55:35 david Exp $
#ifndef __PF2ASM_NODE_logical_XOR_H__
#define __PF2ASM_NODE_logical_XOR_H__

#include <cdk/ast/basic_node.h>
#include "targets/basic_ast_visitor.h"

namespace pf2asm {
  namespace logical {

    class XOR: public cdk::basic_node {
    public:
      inline XOR(int lineno) :
          cdk::basic_node(lineno) {
      }
      inline void accept(basic_ast_visitor *sp, int level) {
        sp->do_XOR(this, level);
      }
    };

  } // logical
} // pf2asm

#endif
