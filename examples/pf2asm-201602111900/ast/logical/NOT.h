// $Id: NOT.h,v 1.1 2014/02/26 18:55:35 david Exp $
#ifndef __PF2ASM_NODE_logical_NOT_H__
#define __PF2ASM_NODE_logical_NOT_H__

#include <cdk/ast/basic_node.h>
#include "targets/basic_ast_visitor.h"

namespace pf2asm {
  namespace logical {

    class NOT: public cdk::basic_node {
    public:
      inline NOT(int lineno) :
          cdk::basic_node(lineno) {
      }
      inline void accept(basic_ast_visitor *sp, int level) {
        sp->do_NOT(this, level);
      }
    };

  } // logical
} // pf2asm

#endif
