// $Id: UGT.h,v 1.1 2014/02/26 18:55:33 david Exp $
#ifndef __PF2ASM_NODE_relational_UGT_H__
#define __PF2ASM_NODE_relational_UGT_H__

#include <cdk/ast/basic_node.h>
#include "targets/basic_ast_visitor.h"

namespace pf2asm {
  namespace relational {

    class UGT: public cdk::basic_node {
    public:
      inline UGT(int lineno) :
          cdk::basic_node(lineno) {
      }
      inline void accept(basic_ast_visitor *sp, int level) {
        sp->do_UGT(this, level);
      }
    };

  } // relational
} // pf2asm

#endif
