// $Id: LT.h,v 1.1 2014/02/26 18:55:33 david Exp $
#ifndef __PF2ASM_NODE_relational_LT_H__
#define __PF2ASM_NODE_relational_LT_H__

#include <cdk/ast/basic_node.h>
#include "targets/basic_ast_visitor.h"

namespace pf2asm {
  namespace relational {

    class LT: public cdk::basic_node {
    public:
      inline LT(int lineno) :
          cdk::basic_node(lineno) {
      }
      inline void accept(basic_ast_visitor *sp, int level) {
        sp->do_LT(this, level);
      }
    };

  } // relational
} // pf2asm

#endif
