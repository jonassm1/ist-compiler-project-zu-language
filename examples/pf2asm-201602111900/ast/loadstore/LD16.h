// $Id: LD16.h,v 1.1 2014/02/26 18:55:15 david Exp $
#ifndef __PF2ASM_NODE_loadstore_LD16_H__
#define __PF2ASM_NODE_loadstore_LD16_H__

#include <cdk/ast/basic_node.h>
#include "targets/basic_ast_visitor.h"

namespace pf2asm {
  namespace loadstore {

    class LD16: public cdk::basic_node {
    public:
      inline LD16(int lineno) :
          cdk::basic_node(lineno) {
      }
      inline void accept(basic_ast_visitor *sp, int level) {
        sp->do_LD16(this, level);
      }
    };

  } // loadstore
} // pf2asm

#endif
