// $Id: ULD16.h,v 1.1 2014/02/26 18:55:16 david Exp $
#ifndef __PF2ASM_NODE_loadstore_ULD16_H__
#define __PF2ASM_NODE_loadstore_ULD16_H__

#include <cdk/ast/basic_node.h>
#include "targets/basic_ast_visitor.h"

namespace pf2asm {
  namespace loadstore {

    class ULD16: public cdk::basic_node {
    public:
      inline ULD16(int lineno) :
          cdk::basic_node(lineno) {
      }
      inline void accept(basic_ast_visitor *sp, int level) {
        sp->do_ULD16(this, level);
      }
    };

  } // loadstore
} // pf2asm

#endif
