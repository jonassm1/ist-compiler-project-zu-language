// $Id: ULDCHR.h,v 1.1 2014/02/26 18:55:16 david Exp $
#ifndef __PF2ASM_NODE_loadstore_ULDCHR_H__
#define __PF2ASM_NODE_loadstore_ULDCHR_H__

#include <cdk/ast/basic_node.h>
#include "targets/basic_ast_visitor.h"

namespace pf2asm {
  namespace loadstore {

    class ULDCHR: public cdk::basic_node {
    public:
      inline ULDCHR(int lineno) :
          cdk::basic_node(lineno) {
      }
      inline void accept(basic_ast_visitor *sp, int level) {
        sp->do_ULDCHR(this, level);
      }
    };

  } // loadstore
} // pf2asm

#endif
