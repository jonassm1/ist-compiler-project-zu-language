// $Id: D2I.h,v 1.1 2014/02/26 18:55:26 david Exp $
#ifndef __PF2ASM_NODE_nodes_D2I_H__
#define __PF2ASM_NODE_nodes_D2I_H__

#include <cdk/ast/basic_node.h>
#include "targets/basic_ast_visitor.h"

namespace pf2asm {

  class D2I: public cdk::basic_node {
  public:
    inline D2I(int lineno) :
        cdk::basic_node(lineno) {
    }
    inline void accept(basic_ast_visitor *sp, int level) {
      sp->do_D2I(this, level);
    }
  };

} // pf2asm

#endif
