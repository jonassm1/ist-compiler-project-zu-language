// $Id: DUP.h,v 1.1 2014/02/26 18:55:26 david Exp $
#ifndef __PF2ASM_NODE_nodes_DUP_H__
#define __PF2ASM_NODE_nodes_DUP_H__

#include <cdk/ast/basic_node.h>
#include "targets/basic_ast_visitor.h"

namespace pf2asm {

  class DUP: public cdk::basic_node {
  public:
    inline DUP(int lineno) :
        cdk::basic_node(lineno) {
    }
    inline void accept(basic_ast_visitor *sp, int level) {
      sp->do_DUP(this, level);
    }
  };

} // pf2asm

#endif
