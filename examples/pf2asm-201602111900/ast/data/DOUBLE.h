// $Id: DOUBLE.h,v 1.1 2014/02/26 18:55:18 david Exp $
#ifndef __PF2ASM_NODE_data_DOUBLE_H__
#define __PF2ASM_NODE_data_DOUBLE_H__

#include <cdk/ast/basic_node.h>
#include "targets/basic_ast_visitor.h"

namespace pf2asm {
  namespace data {

    class DOUBLE: public cdk::basic_node {
      double _value;

    public:
      inline DOUBLE(int lineno, double value) :
          cdk::basic_node(lineno), _value(value) {
      }
      inline double value() const {
        return _value;
      }
      inline void accept(basic_ast_visitor *sp, int level) {
        sp->do_DOUBLE(this, level);
      }
    };

  } // data
} // pf2asm

#endif
