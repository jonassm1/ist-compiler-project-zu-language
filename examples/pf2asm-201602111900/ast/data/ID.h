// $Id: ID.h,v 1.1 2014/02/26 18:55:19 david Exp $
#ifndef __PF2ASM_NODE_data_ID_H__
#define __PF2ASM_NODE_data_ID_H__

#include <cdk/ast/basic_node.h>
#include "targets/basic_ast_visitor.h"

namespace pf2asm {
  namespace data {

    class ID: public cdk::basic_node {
      std::string _value;

    public:
      inline ID(int lineno, const std::string &value) :
          cdk::basic_node(lineno), _value(value) {
      }
      inline const std::string & value() const {
        return _value;
      }
      inline void accept(basic_ast_visitor *sp, int level) {
        sp->do_ID(this, level);
      }
    };

  } // data
} // pf2asm

#endif
