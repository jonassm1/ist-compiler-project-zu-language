// $Id: CHAR.h,v 1.1 2014/02/26 18:55:19 david Exp $
#ifndef __PF2ASM_NODE_data_CHAR_H__
#define __PF2ASM_NODE_data_CHAR_H__

#include <cdk/ast/basic_node.h>
#include "targets/basic_ast_visitor.h"

namespace pf2asm {
    namespace data {

      class CHAR: public cdk::basic_node {
        std::string _value;

      public:
        inline CHAR(int lineno, const std::string &value) :
          cdk::basic_node(lineno), _value(value) {
        }
        inline const std::string & value() const {
          return _value;
        }
        inline void accept(basic_ast_visitor *sp, int level) {
          sp->do_CHAR(this, level);
        }
      };

    } // data
} // pf2asm

#endif
