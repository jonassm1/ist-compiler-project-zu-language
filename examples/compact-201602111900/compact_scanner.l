%option c++ yyclass="compact_scanner" outfile="compact_scanner.cpp"
%option stack noyywrap yylineno 8bit
%{ 
/* $Id: CompactScanner.l,v 1.17 2014/02/20 15:32:02 david Exp $ */
// make relevant includes before including the parser's tab file
#include <string>
#include <cdk/ast/sequence_node.h>
#include <cdk/ast/expression_node.h>
#include "compact_scanner.h"
#include "compact_parser.tab.h"

// don't change this
#define yyerror LexerError
%}
%%

"#".*                  ; /* ignore comments */
">="                   return tGE; 
"<="                   return tLE; 
"=="                   return tEQ; 
"!="                   return tNE; 

"while"                return tWHILE; 
"if"                   return tIF; 
"else"                 return tELSE; 
"print"                return tPRINT; 
"read"                 return tREAD; 
"program"              return tPROGRAM; 
"end"                  return tEND; 

[A-Za-z][A-Za-z0-9_]*  yylval.s = new std::string(yytext); return tIDENTIFIER;

\'[^']*\'              yytext[yyleng-1] = 0; yylval.s = new std::string(yytext+1); return tSTRING;

[0-9]+                 yylval.i = strtol(yytext, nullptr, 10); return tINTEGER;

[-()<>=+*/%;{}.]       return *yytext;

[ \t\n]+               ; /* ignore whitespace */

.                      yyerror("Unknown character");

%%
// Very, very dirty hack: flex is a mess generating C++ scanners.
int compact_scanner::yywrap() { return 1; }
