%{
// $Id: CompactParser.y,v 1.21 2014/02/20 15:32:02 david Exp $
//-- don't change *any* of these: if you do, you'll break the compiler.
#include <cdk/compiler.h>
#include "ast/all.h"
#define LINE       compiler->scanner()->lineno()
#define yylex()    compiler->scanner()->scan()
#define yyerror(s) compiler->scanner()->error(s)
#define YYPARSE_PARAM_TYPE std::shared_ptr<cdk::compiler>
#define YYPARSE_PARAM      compiler
//-- don't change *any* of these --- END!
%}

%union {
  int                   i;	/* integer value */
  std::string          *s;	/* symbol name or string literal */
  cdk::basic_node      *node;	/* node pointer */
  cdk::sequence_node   *sequence;
  cdk::expression_node *expression; /* expression nodes */
};

%token <i> tINTEGER
%token <s> tIDENTIFIER tSTRING
%token tWHILE tIF tPRINT tREAD tPROGRAM tEND
%nonassoc tIFX
%nonassoc tELSE

%left tGE tLE tEQ tNE '>' '<'
%left '+' '-'
%left '*' '/' '%'
%nonassoc tUMINUS

%type <node> stmt program
%type <sequence> list
%type <expression> expr

%{
//-- The rules below will be included in yyparse, the main parsing function.
%}
%%

program	: tPROGRAM list tEND
        {
          compiler->ast(new compact::program_node(LINE, $2));
        }
	      ;

stmt : ';'                   { $$ = new cdk::nil_node(LINE); }
	   | tPRINT tSTRING ';'    { $$ = new cdk::string_node(LINE, $2); }
 	   | tPRINT expr ';'       { $$ = new compact::print_node(LINE, $2); }
     | tREAD tIDENTIFIER ';'
     {
       $$ = new compact::read_node(LINE, new compact::lvalue_node(LINE, $2));
     }
     | tIDENTIFIER '=' expr ';'
     {
       $$ = new compact::assignment_node(LINE, new compact::lvalue_node(LINE, $1), $3);
     }
     | tWHILE '(' expr ')' stmt         { $$ = new compact::while_node(LINE, $3, $5); }
     | tIF '(' expr ')' stmt %prec tIFX { $$ = new compact::if_node(LINE, $3, $5); }
     | tIF '(' expr ')' stmt tELSE stmt { $$ = new compact::if_else_node(LINE, $3, $5, $7); }
     | '{' list '}'                     { $$ = $2; }
     ;

list : stmt	     { $$ = new cdk::sequence_node(LINE, $1); }
	   | list stmt { $$ = new cdk::sequence_node(LINE, $2, $1); }
	   ;

expr : tINTEGER                { $$ = new cdk::integer_node(LINE, $1); }
     | tIDENTIFIER             { $$ = new cdk::identifier_node(LINE, $1); }
     | '-' expr %prec tUMINUS  { $$ = new cdk::neg_node(LINE, $2); }
     | expr '+' expr	         { $$ = new cdk::add_node(LINE, $1, $3); }
     | expr '-' expr	         { $$ = new cdk::sub_node(LINE, $1, $3); }
     | expr '*' expr	         { $$ = new cdk::mul_node(LINE, $1, $3); }
     | expr '/' expr	         { $$ = new cdk::div_node(LINE, $1, $3); }
     | expr '%' expr	         { $$ = new cdk::mod_node(LINE, $1, $3); }
     | expr '<' expr	         { $$ = new cdk::lt_node(LINE, $1, $3); }
     | expr '>' expr	         { $$ = new cdk::gt_node(LINE, $1, $3); }
     | expr tGE expr	         { $$ = new cdk::ge_node(LINE, $1, $3); }
     | expr tLE expr           { $$ = new cdk::le_node(LINE, $1, $3); }
     | expr tNE expr	         { $$ = new cdk::ne_node(LINE, $1, $3); }
     | expr tEQ expr	         { $$ = new cdk::eq_node(LINE, $1, $3); }
     | '(' expr ')'            { $$ = $2; }
     ;

%%
