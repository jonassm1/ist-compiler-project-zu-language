// $Id: c_target.h,v 1.3 2014/03/31 15:52:10 david Exp $
#ifndef __COMPACT_SEMANTICS_C_EVALUATOR_H__
#define __COMPACT_SEMANTICS_C_EVALUATOR_H__

#include <cdk/basic_target.h>
#include <cdk/symbol_table.h>
#include <cdk/ast/basic_node.h>
#include <cdk/compiler.h>
#include "targets/c_writer.h"
#include "targets/symbol.h"

namespace compact {

  class c_target: public cdk::basic_target {
    static c_target _self;

  private:
    inline c_target() :
        cdk::basic_target("c") {
    }

  public:
    bool evaluate(std::shared_ptr<cdk::compiler> compiler) {
      // this symbol table will be used to check identifiers
      // an exception will be thrown if identifiers are used before declaration
      cdk::symbol_table<compact::symbol> symtab;

      // generate C code
      c_writer writer(compiler, symtab);
      compiler->ast()->accept(&writer, 0);

      return true;
    }

  };

} // compact

#endif
