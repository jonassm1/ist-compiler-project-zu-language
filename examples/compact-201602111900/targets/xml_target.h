// $Id: xml_target.h,v 1.3 2014/03/31 15:52:09 david Exp $
#ifndef __COMPACT_SEMANTICS_XML_EVALUATOR_H__
#define __COMPACT_SEMANTICS_XML_EVALUATOR_H__

#include <cdk/basic_target.h>
#include <cdk/compiler.h>
#include "targets/xml_writer.h"

namespace compact {

  class xml_target: public cdk::basic_target {
    static xml_target _self;

  private:
    inline xml_target() :
        cdk::basic_target("xml") {
    }

  public:
    bool evaluate(std::shared_ptr<cdk::compiler> compiler) {
      xml_writer writer(compiler);
      compiler->ast()->accept(&writer, 0);
      return true;
    }

  };

} // compact

#endif
