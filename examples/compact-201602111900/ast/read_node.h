// $Id: read_node.h,v 1.4 2014/03/31 15:52:10 david Exp $ -*- c++ -*-
#ifndef __COMPACT_READNODE_H__
#define __COMPACT_READNODE_H__

#include "ast/lvalue_node.h"

namespace compact {

  /**
   * Class for describing read nodes.
   */
  class read_node: public cdk::basic_node {
    compact::lvalue_node *_argument;

  public:
    inline read_node(int lineno, compact::lvalue_node *argument) :
        cdk::basic_node(lineno), _argument(argument) {
    }

  public:
    inline compact::lvalue_node *argument() {
      return _argument;
    }

    void accept(basic_ast_visitor *sp, int level) {
      sp->do_read_node(this, level);
    }

  };

} // compact

#endif
