// $Id: assignment_node.h,v 1.4 2014/03/31 15:52:10 david Exp $ -*- c++ -*-
#ifndef __COMPACT_ASSIGNMENTNODE_H__
#define __COMPACT_ASSIGNMENTNODE_H__

#include "ast/lvalue_node.h"

namespace compact {

  /**
   * Class for describing assignment nodes.
   */
  class assignment_node: public cdk::basic_node {
    compact::lvalue_node *_lvalue;
    cdk::expression_node *_rvalue;

  public:
    inline assignment_node(int lineno, compact::lvalue_node *lvalue, cdk::expression_node *rvalue) :
        cdk::basic_node(lineno), _lvalue(lvalue), _rvalue(rvalue) {
    }

  public:
    inline compact::lvalue_node *lvalue() {
      return _lvalue;
    }
    inline cdk::expression_node *rvalue() {
      return _rvalue;
    }

    void accept(basic_ast_visitor *sp, int level) {
      sp->do_assignment_node(this, level);
    }

  };

} // compact

#endif
