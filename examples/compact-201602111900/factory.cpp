// $Id: factory.cpp,v 1.4 2014/02/25 21:42:29 david Exp $ -*- c++ -*-

#include "factory.h"

/**
 * This object is automatically registered by the constructor in the
 * superclass' language registry.
 */
compact::factory compact::factory::_self;
