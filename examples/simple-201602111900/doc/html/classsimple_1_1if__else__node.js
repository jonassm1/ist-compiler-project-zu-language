var classsimple_1_1if__else__node =
[
    [ "if_else_node", "classsimple_1_1if__else__node.html#a4a3d16100befd1872084ea8ccd7587ad", null ],
    [ "accept", "classsimple_1_1if__else__node.html#a06de25c0657ddd9c8d4a9340d0fc57a0", null ],
    [ "condition", "classsimple_1_1if__else__node.html#a0079e9c582acbe4575734ec7341b7977", null ],
    [ "elseblock", "classsimple_1_1if__else__node.html#a51c14ec9c3d19e5e45a8bdeaa999df72", null ],
    [ "thenblock", "classsimple_1_1if__else__node.html#a5f440f43ff524610b4ad6a576fd1a18a", null ],
    [ "_condition", "classsimple_1_1if__else__node.html#a3b75d311e7421c64e319c2d282ddd9ee", null ],
    [ "_elseblock", "classsimple_1_1if__else__node.html#a6bfe3fdc182b3ce944daa73f189dad9c", null ],
    [ "_thenblock", "classsimple_1_1if__else__node.html#a7ed9bfb33a79855fa9160000a40a87ad", null ]
];